{ mkDerivation, full, qtbase, qtwebsockets, qmake, libsodium, zcash, fetchFromGitHub, stdenv, writeTextFile, gnome3 }:
let
  desktopFile = writeTextFile { 
    name = "zecwallet.desktop";
    text = ''
      [Desktop Entry]
      Name=ZECWallet
      Exec=zecwallet
      Icon=zecwallet
      Comment=GUI Wallet for ZCash
      '';
  };
in mkDerivation rec {
  name = "zecwallet-${version}";
  src = fetchFromGitHub {
    owner = "ZcashFoundation";
    repo = "zecwallet";
    rev = "v${version}";
    sha256 = "0srqhh9s6mrjjj6anx8235ld9c928ix25pv7wxbdybhra8mx06cl";
  };
  version = "0.8.0";

  buildInputs = [ qmake qtbase qtwebsockets zcash libsodium ];

  # Use nix libsodium instead of building it in0place
  patches = [ ./zec-qt-wallet.pro.patch ];
  qmakeFlags = [ "LIBS+=-L${libsodium}/lib" "LIBS+=-lsodium"];

  preConfigure = ''
    # Generate translations
    QT_STATIC=${full} bash src/scripts/dotranslations.sh

    # Add embedded zcashd
    mkdir -p artifacts
    cp ${zcash}/bin/zcashd ${zcash}/bin/zcash-cli artifacts
    '';

  postInstall = ''
    # Copy exes for embedded zcashd
    cp artifacts/zcashd artifacts/zcash-cli $out/bin

    # Install desktop file and icon
    mkdir -p $out/share/applications
    mkdir -p $out/share/icons/hicolor/48x48/apps
    cp res/logo.svg $out/share/icons/hicolor/48x48/apps/zecwallet.svg
    cp ${desktopFile} $out/share/applications/zecwallet.desktop
    '';

  passthru = {
    updateScript = gnome3.updateScript {
      packageName = "zecwallet";
    };
  };
  meta = with stdenv.lib; {
    description = "QT ZCash wallet with embedded zcashd";
    homepage = "https://github.com/ZcashFoundation/zecwallet";
    license = licenses.mit;
    maintainers = [ maintainers.eamsden ];
    platforms = platforms.all;
  };
}
