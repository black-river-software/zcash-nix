{ openssl, cyrus_sasl, jsoncpp, python, ruby, cmake, stdenv, fetchurl }:
stdenv.mkDerivation rec {
  name = "qpid-proton-${version}";
  version = "0.29.0";
  patches = [ ./qpid-proton-cmakelists.patch ];
  patchFlags = [ "-p2" ];
  src = fetchurl {
    url = "https://archive.apache.org/dist/qpid/proton/${version}/qpid-proton-${version}.tar.gz";
    sha512 = "d6b7bc9811230c48b0c10c3cc076a6de6b29c5e22d69556f83e21110eeb9a105d11328b2993694a2e39f0681007b4b3144270ea18efd456a802a08cb564ae4a4";
  };
  cmakeFlags = "-DBUILD_PYTHON=OFF -DBUILD_RUBY=OFF -DCMAKE_CXX_FLAGS=-Wno-format-security -DENABLE_FUZZ_TESTING=OFF -DBUILD_STATIC_LIBS=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_CXX_STANDARD=11";
  nativeBuildInputs = [ cmake python ruby ];
  buildInputs = [ openssl cyrus_sasl jsoncpp ];

  meta = with stdenv.lib; {
    description = "Apache QPID Proton messaging library";
    homepage = "https://qpid.apache.org/proton/";
    license = licenses.asl20;
    maintainers = [ maintainers.eamsden ];
    platforms = platforms.all;
  };

}

