{rustPlatform, stdenv, fetchFromGitHub}:
rustPlatform.buildRustPackage rec {
  name = "librustzcash-${version}";
  version = "0.2.0";

  src = fetchFromGitHub {
    owner = "zcash";
    repo = "librustzcash";
    rev = "06da3b9ac8f278e5d4ae13088cf0a4c03d2c13f5";
    sha256 = "0md0pp3k97iv7kfjpfkg14pjanhrql4vafa8ggbxpkajv1j4xldv";
  };

  cargoSha256 = "166v8cxlpfslbs5gljbh7wp0lxqakayw47ikxm9r9a39n7j36mq1";

  doCheck = false;

  postInstall = ''
    cp -R ${src}/librustzcash/include $out/include
    '';

  meta = with stdenv.lib; {
    description = "Rust components for zcashd";
    homepage = "https://github.com/zcash/librustzcash";
    license = licenses.asl20;
    maintainers = [ maintainers.eamsden ];
    platforms = platforms.all;
  };
}
