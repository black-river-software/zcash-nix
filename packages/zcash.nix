{ db62 # Berkeley DB version 6.2
, boost170
, gmock
, libevent
, librustzcash
, libsodium
, openssl
, qpid-proton
, zeromq
, hexdump
, autoreconfHook
, pkg-config
, stdenv
, fetchFromGitHub }:
let
  boost-static = boost170.override { enableStatic = true; };
in stdenv.mkDerivation rec {
  name = "zcash-${version}";
  version = "2.1.0-1";
  src = fetchFromGitHub {
    owner = "zcash";
    repo = "zcash";
    rev = "v${version}";
    sha256 = "05bnn4lxrrcv1ha3jdfrgwg4ar576161n3j9d4gpc14ww3zgf9vz";
  };

  buildInputs = [ autoreconfHook pkg-config hexdump db62 boost-static gmock libevent librustzcash libsodium openssl qpid-proton zeromq ];

  configureFlags = "--with-boost-libdir=${boost-static}/lib";

  meta = with stdenv.lib; {
    description = "ZCash daemon and command-line client";
    homepage = "https://github.com/zcash/zcash";
    license = licenses.mit;
    maintainers = [ maintainers.eamsden ];
    platforms = platforms.all;
  };
}


