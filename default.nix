{ sources ? import nix/sources.nix }:
let
  overlays = import ./overlays.nix;
in import sources.nixpkgs { inherit overlays; config = {}; }

